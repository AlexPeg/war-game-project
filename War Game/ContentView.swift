//
//  ContentView.swift
//  War Game
//
//  Created by Alex Pégourié on 11/09/2021.
//

import SwiftUI

struct ContentView: View {
    
   @State private var playerCard = "card5"
   @State private var cpuCard = "card9"
   @State private var playerScore = 0
   @State private var cpuScore = 0
    
    @State private var showPopUp = false
    @State private var popUpTitle = ""
    @State private var popUpContent = ""
    
    var body: some View {
        ZStack{
            Image("background")
                .ignoresSafeArea()
            VStack{
                Spacer()
                Image("logo")
                Spacer()
                
                if self.showPopUp == true {
                    ZStack {
                        Color.white
                        VStack {
                            Text(popUpTitle)
                            Spacer()
                            Text(popUpContent)
                            .font(.largeTitle)
                            Spacer()
                            Button(action: {
                                self.showPopUp = false
                                cpuScore = 0;
                                playerScore = 0;
                            }, label: {
                                Text("Close")
                            })
                        }.padding()
                    }
                    .frame(width: 300, height: 200)
                    .cornerRadius(20).shadow(radius: 20)
                    Spacer()
                } else {
                
                HStack{
                    Spacer()
                    Image(playerCard)
                    Spacer()
                    Image(cpuCard)
                    Spacer()
                    
                    
                }
                Spacer()
                Button(action: {
                    
                    // Generate a random number between 2 and 14
                    let playerRand = Int.random(in: 2...14)
                    let cpuRand = Int.random(in: 2...14)
                    // Update the cards
                    playerCard = "card" + String(playerRand)
                    cpuCard = "card" + String(cpuRand)
                    
                    // Update the score
                    if playerRand > cpuRand {
                        playerScore += 1
                    }
                    else if cpuRand > playerRand {
                        cpuScore += 1
                    }
                    
                    if cpuScore == 10 || playerScore == 10 {
                        self.showPopUp = true
                        popUpTitle = "Game"
                        
                        if cpuScore < playerScore {
                        popUpContent = "Player won"
                        } else if cpuScore > playerScore {
                        popUpContent = "CPU won"
                        } else if cpuScore == playerScore && cpuScore == 10 && playerScore == 10{
                            popUpContent = "Match null"
                        }
                        
                    }
                    
                }, label: {
                    Image("dealbutton")

                })
                }
                
                Spacer()
                HStack{
                    Spacer()
                    VStack{
                        Text("Player")
                            .font(.largeTitle)
                            .foregroundColor(Color.white)
                            .padding(10.0)
                        Text(String(playerScore))
                            .font(.largeTitle)
                            .foregroundColor(Color.white)
                    }
                    Spacer()
                    VStack{
                        Text("CPU")
                            .font(.largeTitle)
                            .foregroundColor(Color.white)
                            .padding(10.0)
                        Text(String(cpuScore))
                            .font(.largeTitle)
                            .foregroundColor(Color.white)
                    }
                    Spacer()
                }
                Spacer()
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
