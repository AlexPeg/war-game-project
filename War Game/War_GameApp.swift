//
//  War_GameApp.swift
//  War Game
//
//  Created by Lola Garavagno on 11/09/2021.
//

import SwiftUI

@main
struct War_GameApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
